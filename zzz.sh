#!/bin/bash

#Menu de Escolhas

sleep 1
read -p "Digite o nome do arquivo .zip (Se o arquivo ainda não existe, Digite N para criar um novo): " az

if [ $az = 'N' ]; then
	read -p "Digite o nome do zip a ser criado: " nz
	az="$nz"
	read -p "DIgite o nome dos arquivos a serem compactados: " ac
	zip $az $ac
fi

echo "Escolha uma das opções disponíveis: "
echo "1. Listar os arquivos compactados do arquivo .zip"
echo "2. Pré-visualizar o conteúdo do arquivo .zip"
echo "3. Adicionar arquivos"
echo "4. Remover arquivos"
echo "5. Extrair arquivos"
echo "6. Extrair um arquivo específico"

read -p "Digite a operação desejada: " o

#Listando o conteúdo do arquivo zip

if [ $o -eq "1" ]; then
	unzip -l $az
fi

#Pré-visualizando o conteúdo de algum arquivo zip compactado

if [ $o -eq "2" ]; then
	read -p "Digite o nome do arquivo que você deseja fazer a pré-visualização: " pv
	unzip -p $az $pv
fi

#Adicionando arquivos ao zip
if [ $o -eq "3" ]; then
	read -p "Digite o nome do arquivo que você deseja adicionar ao .zip: " na
	zip $az $na
fi

#Removendo arquivos do zip
if [ $o -eq "4" ]; then
	read -p "Digite o nome do arquivo que você deseja remover do .zip: " ar
	zip -d $az $ar 
fi

#Extraindo todo o conteúdo do zip
if [ $o -eq "5" ]; then
	unzip $az
fi


#Extraíndo arquivos específicos do zip
if [ $o -eq "6" ]; then
	read -p "Digite o nome do arquivo a ser extraído: " ae
	unzip $az $ae
fi
